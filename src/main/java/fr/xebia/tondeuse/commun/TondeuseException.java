package fr.xebia.tondeuse.commun;

import org.apache.commons.lang3.StringUtils;

public class TondeuseException extends RuntimeException {

	/**
	 * serial UID
	 */
	private static final long serialVersionUID = 1L;

	public TondeuseException(int code, String message) {

		super(ExceptionEnum.getExceptionEnum(code).getMessage() + StringUtils.SPACE + message);

	}
	
	public TondeuseException(Exception e) {
		super(e.getMessage());
	}

}
