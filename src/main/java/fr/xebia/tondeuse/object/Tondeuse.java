package fr.xebia.tondeuse.object;

import java.util.ArrayList;
import java.util.Collection;
import java.util.concurrent.locks.Lock;

import fr.xebia.tondeuse.commun.OrientationEnum;
import fr.xebia.tondeuse.service.ActionTondeuseService;
import fr.xebia.tondeuse.service.impl.ActionTondeuseServiceImpl;
import javafx.animation.Timeline;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;


public class Tondeuse extends Thread{
	
	private int numero;
	
	private int positionX;
	
	private int positionY;
	
	private OrientationEnum orientation;
	
	private String instructions;
	
	//Quand une tondeuse bouge elle informe toutes les autres
	private Collection<Tondeuse> tondeuseAbonnee;
	
	private Lock verrou;
	
	
	public Tondeuse(){
		this.tondeuseAbonnee = new ArrayList<Tondeuse>();
	}
	
	public void ajouterTondeuse(Tondeuse tondeuse){
		tondeuseAbonnee.add(tondeuse);
	}
	
	
	public int getNumero() {
		
		return numero;
	}

	public void setNumero(int numero) {
		this.numero = numero;
	}

	public int getPositionX() {
		return positionX;
	}

	public void setPositionX(int positionX) {
		this.positionX = positionX;
	}

	public int getPositionY() {
		return positionY;
	}

	public void setPositionY(int positionY) {
		this.positionY = positionY;
	}

	public OrientationEnum getOrientation() {
		return orientation;
	}

	public void setOrientation(OrientationEnum orientation) {
		this.orientation = orientation;
	}

	public String getInstructions() {
		return instructions;
	}

	public void setInstructions(String instructions) {
		this.instructions = instructions;
	}


	@Override
	public void run() {
		//On fait appel à un service car on peut envisager plusieurs façon de faire bouger la tondeuse
		ActionTondeuseService action = new ActionTondeuseServiceImpl();
		action.bougerTondeuse(this);
	}

	public Collection<Tondeuse> getTondeuseAbonnee() {
		return tondeuseAbonnee;
	}

	public Lock getVerrou() {
		return verrou;
	}

	public void setVerrou(Lock verrou) {
		this.verrou = verrou;
	}


 

	
	
	
	
}
