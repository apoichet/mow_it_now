package fr.xebia.tondeuse.object;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import fr.xebia.tondeuse.service.LireInsctructionService;
import fr.xebia.tondeuse.service.impl.LireInstructionServiceImpl;
import javafx.stage.Stage;

public class Surface{
	private static Surface uniqueSurface;

	private int largeur;

	private int longueur;

	private Collection<Tondeuse> tondeuses;

	private Surface() {
		// On lit le fichier à ce moment là
		// L'utilisation d'un service permet d'envisager d'autres façon de lire
		// le fichier
		LireInsctructionService insctructionService = new LireInstructionServiceImpl();
		Map<String, Object> mapInstruct = insctructionService.getMapInstruct();
		this.largeur = insctructionService.getLargeurSurface(mapInstruct);
		this.longueur = insctructionService.getLongueur(mapInstruct);
		this.tondeuses = insctructionService.getTondeuse(mapInstruct);
	}

	//Au chargement de la classe
	static{
		uniqueSurface = new Surface();
	}
	
	
	public static Surface getSurface() {
		return uniqueSurface;

	}

	public int getLargeur() {
		return largeur;
	}

	public int getLongueur() {
		return longueur;
	}

	public Collection<Tondeuse> getTondeuses() {
		return tondeuses;
	}




}
