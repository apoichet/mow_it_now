package fr.xebia.tondeuse.service;

import fr.xebia.tondeuse.object.Tondeuse;

public interface ActionTondeuseService {
	public void bougerTondeuse(Tondeuse tondeuseMoove);
}
