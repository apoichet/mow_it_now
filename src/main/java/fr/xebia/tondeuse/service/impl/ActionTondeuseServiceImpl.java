package fr.xebia.tondeuse.service.impl;

import java.util.Collection;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import fr.xebia.tondeuse.commun.OrientationEnum;
import fr.xebia.tondeuse.commun.TondeuseUtils;
import fr.xebia.tondeuse.object.Surface;
import fr.xebia.tondeuse.object.Tondeuse;
import fr.xebia.tondeuse.service.ActionTondeuseService;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.scene.transform.Rotate;
import javafx.util.Duration;

public class ActionTondeuseServiceImpl implements ActionTondeuseService {

	@Override
	public void bougerTondeuse(Tondeuse tondeuseMoove) {

		String instructions = tondeuseMoove.getInstructions();

		if (StringUtils.isNotEmpty(instructions)) {

			char tabInsctruct[] = instructions.toCharArray();

			for (char instruct : tabInsctruct) {

				traiterInstruct(tondeuseMoove, instruct);

			}

		}

	}

	protected void traiterInstruct(Tondeuse tondeuseMoove, char instruction) {

		
		int coordonnee[] = new int[2];
		coordonnee[0] = tondeuseMoove.getPositionX();
		coordonnee[1] = tondeuseMoove.getPositionY();
		OrientationEnum orientation = tondeuseMoove.getOrientation();

		switch (instruction) {
		case TondeuseUtils.CODE_AVANCER:
			tondeuseMoove.getVerrou().lock();
			System.out.println(tondeuseMoove.getNumero()+" avance");
			coordonnee = avancerTondeuse(tondeuseMoove);
			tondeuseMoove.setPositionX(coordonnee[0]);
			tondeuseMoove.setPositionY(coordonnee[1]);
			System.out.println(tondeuseMoove.getNumero()+" a fini d'avancer");
			tondeuseMoove.getVerrou().unlock();
			break;

		case TondeuseUtils.CODE_DROITE:

			orientation = OrientationEnum.getOrientationEnumRight(orientation);
			break;

		case TondeuseUtils.CODE_GAUCHE:

			orientation = OrientationEnum.getOrientationEnumLeft(orientation);
			break;
		}

		tondeuseMoove.setOrientation(orientation);

	}

	protected int[] avancerTondeuse(Tondeuse tondeuseMoove) {
		OrientationEnum orientation = tondeuseMoove.getOrientation();
		int positionX = tondeuseMoove.getPositionX();
		int positionY = tondeuseMoove.getPositionY();

		int positionFinalX = tondeuseMoove.getPositionX();
		int positionFinalY = tondeuseMoove.getPositionY();

		if (OrientationEnum.NORD.equals(orientation)) {
			positionY++;

		} else if (OrientationEnum.SUD.equals(orientation)) {
			positionY--;

		} else if (OrientationEnum.WEST.equals(orientation)) {
			positionX--;

		} else if (OrientationEnum.EST.equals(orientation)) {
			positionX++;

		}

		int postionTestX = positionX;
		int positionTestY = positionY;

		if (isOkDeplacement(postionTestX, positionTestY, tondeuseMoove)) {
			positionFinalX = positionX;
			positionFinalY = positionY;
		}

		int coordonne[] = { positionFinalX, positionFinalY };
		
		return coordonne;

	}

	protected boolean isOkDeplacement(int positionX, int positionY, Tondeuse tondeuseMoove) {

		boolean okFrontiere = isOkFrontiereSurface(positionX, positionY);

		boolean pasColision = !isColision(positionX, positionY, tondeuseMoove);

		return okFrontiere && pasColision;

	}

	protected boolean isOkFrontiereSurface(int positionX, int positionY) {

		Surface surface = Surface.getSurface();

		boolean ok = false;

		if (surface != null) {

			boolean okLargeur = !depassement(positionX, surface.getLargeur());
			boolean okLongeur = !depassement(positionY, surface.getLongueur());

			ok = okLargeur && okLongeur;

		}

		return ok;
	}

	protected boolean isColision(int positionX, int positionY, Tondeuse tondeuseMoove) {
		boolean colision = false;

		Collection<Tondeuse> tondeuseAbonnees = tondeuseMoove.getTondeuseAbonnee();
		
		if (CollectionUtils.isNotEmpty(tondeuseAbonnees)) {
			
			for (Tondeuse tondeuseAbonnee : tondeuseAbonnees) {
				
				if (positionX == tondeuseAbonnee.getPositionX() && positionY == tondeuseAbonnee.getPositionY()) {
					colision = true;
					System.out.println("Risque de colision évité : "+tondeuseMoove.getNumero()+" dans "+
					tondeuseAbonnee.getNumero());
					break;
				}
				
			}
		}

		return colision;

	}

	protected boolean depassement(int position, int surface) {
		boolean depasse = false;

		if (surface < position || position < 0) {

			depasse = true;

		}

		return depasse;
	}

}
