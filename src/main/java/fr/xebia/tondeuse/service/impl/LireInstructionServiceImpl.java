package fr.xebia.tondeuse.service.impl;

import java.io.*;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;

import fr.xebia.tondeuse.commun.OrientationEnum;
import fr.xebia.tondeuse.commun.TondeuseException;
import fr.xebia.tondeuse.commun.TondeuseUtils;
import fr.xebia.tondeuse.object.Tondeuse;
import fr.xebia.tondeuse.service.LireInsctructionService;
import javafx.animation.Timeline;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

public class LireInstructionServiceImpl implements LireInsctructionService {

	private BufferedReader br;

	@Override
	public Map<String, Object> getMapInstruct() {
		Map<String, Object> mapInstruct = new HashMap<>();

		try {
			URL resource = getClass().getClassLoader().getResource(TondeuseUtils.NOM_FICHIER);
			InputStream ips = new FileInputStream(resource.getFile());
			InputStreamReader ipsr = new InputStreamReader(ips);
			br = new BufferedReader(ipsr);
			String ligne;
			
			int compteurLigne = 1;

			while ((ligne = br.readLine()) != null) {

				if (StringUtils.isNotEmpty(ligne)) {
					String ligneOk = cleanLine(ligne);
					traiterLigne(compteurLigne, ligneOk, mapInstruct);
					
				} else {
					StringBuilder sbLigneVide = new StringBuilder("La ligne n°");
					sbLigneVide.append(compteurLigne);
					sbLigneVide.append(StringUtils.SPACE);
					sbLigneVide.append("est vide !");
					throw new TondeuseException(2, sbLigneVide.toString());
				}

				compteurLigne++;

			}
			br.close();
		} 
		catch(TondeuseException e){
			throw new TondeuseException(e);
		}
		catch (IOException e) {
			throw new TondeuseException(2, "fichier ilisible ou introuvable");
		}
		return mapInstruct;
	}

	protected String cleanLine(String ligneFichier) {
		// On redéfinit la ligne de lecture
		String ligneStrip = StringUtils.strip(ligneFichier);
		String ligneUpper = StringUtils.upperCase(ligneStrip);
		String ligneOk = StringUtils.replace(ligneUpper, StringUtils.SPACE, StringUtils.EMPTY);

		return ligneOk;
	}

	protected void traiterLigne(int numeroLigne, String ligneOk, Map<String, Object> mapInstruct) {
		if (numeroLigne == 1) {// 1ère ligne correspond aux dimensions de la
								// surface

			// On définit les dimensions de la surface
			definirDimensionSurface(ligneOk, numeroLigne, mapInstruct);

		} else {// Les autres sont des tondeuses

			// On ajoute une tondeuse
			definirTondeuse(ligneOk, numeroLigne, mapInstruct);

		}

	}

	protected void definirTondeuse(String instructTondeuse, int compteur, Map<String, Object> mapInstruct) {
		int mapSize = mapInstruct.size();
		int numeroTondeuse = compteur - mapSize + 1;
		String keyTondeuse = String.valueOf(numeroTondeuse);

		Tondeuse tondeuse = (Tondeuse) mapInstruct.get(keyTondeuse);

		if (tondeuse == null) {

			// Position et orientation
			String[] tabPositionOrientation = getPositionOrientationTondeuse(instructTondeuse);
			int positionX = TondeuseUtils.formatingCaractere(tabPositionOrientation[0], compteur);
			int positionY = TondeuseUtils.formatingCaractere(tabPositionOrientation[1], compteur);
			OrientationEnum orientation = OrientationEnum.getOrientationEnum(tabPositionOrientation[2]);

			tondeuse = new Tondeuse();
			tondeuse.setNumero(numeroTondeuse);
			tondeuse.setPositionX(positionX);
			tondeuse.setPositionY(positionY);
			tondeuse.setOrientation(orientation);
			mapInstruct.put(keyTondeuse, tondeuse);

		} else {
			tondeuse.setInstructions(instructTondeuse);

		}

	}

	protected void definirDimensionSurface(String dimensions, int compteur, Map<String, Object> mapInstruct) {
		if (dimensions.length() == 2) {
			String tabDimension[] = dimensions.split(StringUtils.EMPTY);
			String largeurStr = tabDimension[0];
			String longueurStr = tabDimension[1];
			int largeur = TondeuseUtils.formatingCaractere(largeurStr, compteur);
			mapInstruct.put(TondeuseUtils.KEY_SURFACE_LARGEUR, largeur);
			int longueur = TondeuseUtils.formatingCaractere(longueurStr, compteur);
			mapInstruct.put(TondeuseUtils.KEY_SURFACE_LONGUEUR, longueur);
		}
	}

	@Override
	public int getLargeurSurface(Map<String, Object> mapInstruct) {
		int largeur = TondeuseUtils.LARGEUR_SURFACE_DEFAUT;
		if (MapUtils.isNotEmpty(mapInstruct)) {
			largeur = (int) mapInstruct.get(TondeuseUtils.KEY_SURFACE_LARGEUR);
		}
		return largeur;
	}

	@Override
	public int getLongueur(Map<String, Object> mapInstruct) {
		int longueur = TondeuseUtils.LONGUEUR_SURFACE_DEFAUT;
		if (MapUtils.isNotEmpty(mapInstruct)) {
			longueur = (int) mapInstruct.get(TondeuseUtils.KEY_SURFACE_LONGUEUR);
		}
		return longueur;
	}

	@Override
	public Collection<Tondeuse> getTondeuse(Map<String, Object> mapInstruct) {
		Collection<Tondeuse> tondeuses = new ArrayList<>();
		if (MapUtils.isNotEmpty(mapInstruct)) {

			// Les tondeuses
			for (Map.Entry<String, Object> entry : mapInstruct.entrySet()) {

				if (entry != null && entry.getValue() instanceof Tondeuse) {

					Tondeuse tondeuse = (Tondeuse) entry.getValue();
					int positionX = tondeuse.getPositionX();
					int positionY = tondeuse.getPositionY();
					
					tondeuses.add(tondeuse);

				}

			}

		}
		transformTondeuses(tondeuses);
		return tondeuses;
	}

	protected String[] getPositionOrientationTondeuse(String ligneFichier) {
		String[] tabCoordonnee = { TondeuseUtils.POSITION_X_DEFAUT, TondeuseUtils.POSITION_Y_DEFAUT,
				TondeuseUtils.ORIENTATION_DEFAUT };// Par défaut

		if (StringUtils.isNotEmpty(ligneFichier)) {
			char tabChar[] = ligneFichier.toCharArray();

			if (tabChar.length == 3) {

				tabCoordonnee[0] = Character.toString(tabChar[0]);
				tabCoordonnee[1] = Character.toString(tabChar[1]);
				tabCoordonnee[2] = Character.toString(tabChar[2]);

			}

		}

		return tabCoordonnee;
	}

	protected void transformTondeuses(Collection<Tondeuse> tondeuses){
		if (CollectionUtils.isNotEmpty(tondeuses)) {
			for (Tondeuse tondeuse : tondeuses) {
				transformTondeuse(tondeuse, tondeuses);
			}
			
		}
	}
	
	protected void transformTondeuse(Tondeuse tondeuse, Collection<Tondeuse> tondeuses){
		
		for (Tondeuse tondeuseAutre : tondeuses) {
			
			if (tondeuse.getNumero() != tondeuseAutre.getNumero()) {
				
				tondeuse.ajouterTondeuse(tondeuseAutre);
				
			}
			
		}
		
	}

}
