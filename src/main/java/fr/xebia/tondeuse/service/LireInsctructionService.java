package fr.xebia.tondeuse.service;

import java.util.Collection;
import java.util.Map;

import fr.xebia.tondeuse.object.Tondeuse;

public interface LireInsctructionService {

	Map<String, Object> getMapInstruct();
	
	int getLargeurSurface(Map<String, Object> mapInstruct);
	
	int getLongueur(Map<String, Object> mapInstruct);
	
	Collection<Tondeuse> getTondeuse(Map<String, Object> mapInstruct);
}
