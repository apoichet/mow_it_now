package fr.xebia.tondeuse.main;

import java.util.Collection;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import org.apache.commons.collections4.CollectionUtils;

import fr.xebia.tondeuse.object.Surface;
import fr.xebia.tondeuse.object.Tondeuse;
import javafx.application.Application;
import javafx.stage.Stage;

public class Lanceur{

	public static void main(String[] args) {

		Surface surface = Surface.getSurface();

		Lock verrou = new ReentrantLock();
		Collection<Tondeuse> tondeuses = surface.getTondeuses();

		if (CollectionUtils.isNotEmpty(tondeuses)) {
			for (Tondeuse tondeuse : tondeuses) {
				tondeuse.setVerrou(verrou);
				tondeuse.start();
			}
		}
	}
}
