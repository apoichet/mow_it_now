package fr.xebia.tondeuse.service.impl;

import static org.junit.Assert.*;

import java.util.Map;

import org.junit.Before;
import org.junit.Test;

public class LireInstructionServiceImplTest {

	LireInstructionServiceImpl lireInstructService;
	
	@Before
	public void beforeTest(){
		lireInstructService = new LireInstructionServiceImpl();
	}
	
	@Test
	public void getMapInstructTest(){
		Map<String, Object> mapInstruct = lireInstructService.getMapInstruct();
		assertNotNull(mapInstruct);
	}
	
}
